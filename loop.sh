#!/bin/bash

#Initialize the counter

counter=1

#Iterate the loop until the $counter value is less than or equal to 20

while [ $counter -le 20 ]

do

#Print the even numbers

if [[ $counter%2 -eq 0 ]]

then

#Print $counter without newline

echo  "$counter"

fi

#Increment $counter by 1

((counter++))

done